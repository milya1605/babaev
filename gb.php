<?php
$page = "gb";
include "/config.php";

if (isset($_POST['submit'])) {
	$result = gb($_POST['name'], $_POST['email'], $_POST['text']);
	if ($result) {
		$_SESSION['msg'] = "<p style='color:green; font-weight:bold;'>Вы успешно оставили свой отзыв</p>";
		header("Location:gb.php");
		exit();
	}else{
		$_SESSION['msg'] = "<p style='color:red; font-weight:bold;'>Не удалось сохранить Ваш отзыв</p>";
		header("Location:gb.php");
		exit();
	}
}
?>
<!doctype html>
<html>
<head>
	<title><?=$pages['title'];?></title>
	<link href="css/style.css" rel="stylesheet">
</head>

<body>
	<table id="header">
		<tr>
			<td>
				<a href="/"><img src="images/logo.png" alt="" class="logo"></a>
				<div class="logo-text">Кондитерский концерн <br> БАБАЕВСКИЙ</div>				
			</td>
		</tr>
	</table>
	<table id="container">
		<tr>
			<td id="menu" valign="top">
				<ul>
					<li><a href="/">Главная</a></li>
					<li><a href="/catalog.php">Каталог</a></li>
					<li><a href="/news.php">Новости</a></li>
					<li><a href="/gb.php">Гостевая книга</a></li>
					<li><a href="/kontakty.php">Контакты</a></li>
				</ul>

			</td>
			<td id="content" valign="top">
				<h1><?=$pages['h1'];?></h1>
				<?=$pages['text'];?>
				<?=$_SESSION['msg'];?>
				<form action="" method="post">
					<div class="group">
						<label for="name">Ваше имя:</label>
						<input type="text" name="name" id="name">
					</div>
					<div class="group">
						<label for="email">Ваш email</label>
						<input type="text" name="email" id="email">
					</div>
					<div class="group">
						<label for="text">Ваш отзыв</label>
						<input type="text" name="text" id="text">
					</div>
					<input type="submit" name="submit" value="Отправить вопрос">
				</form>

				<div class="otzyvy">
					<? foreach($otzyvy as $item):?>
					<div class="item">
						<div class="name"><?=$item['user'];?></div>
						<div class="email"><?=$item['email'];?> | <?=$item['date'];?></div>
						<div class="text"><?=$item['text'];?></div>
					</div>
					<? endforeach;?>
					
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2"><hr></td>
		</tr>
		<tr id="footer">
			<td class="copy">2016 &copy; Кондитерский концерн "Бабаевский"</td>
		</tr>
	</table>
</body>
</html>
<?
unset($_SESSION['msg']);
?>