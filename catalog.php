<?php
$page = "catalog";
include "/config.php";
?>
<!doctype html>
<html>
<head>
	<title><?=$pages['title'];?></title>
	<link href="css/style.css" rel="stylesheet">
</head>

<body>
	<table id="header">
		<tr>
			<td>
				<a href="/"><img src="images/logo.png" alt="" class="logo"></a>
				<div class="logo-text">Кондитерский концерн <br> БАБАЕВСКИЙ</div>				
			</td>
		</tr>
	</table>
	<table id="container">
		<tr>
			<td id="menu" valign="top">
				<ul>
					<li><a href="/">Главная</a></li>
					<li><a href="/catalog.php">Каталог</a></li>
					<li><a href="/news.php">Новости</a></li>
					<li><a href="/gb.php">Гостевая книга</a></li>
					<li><a href="/kontakty.php">Контакты</a></li>
				</ul>

			</td>
			<td id="content" valign="top">
				<h1><?=$pages['h1'];?></h1>
				<?=$pages['text'];?>
				<div class="catalog">
					<? foreach($catalogs as $item):?>
					<div class="item">
						<div class="header-item"><?=$item['name'];?></div>
						<div class="img-item"><img src="images/catalog/<?=$item['img'];?>.jpg" alt=""></div>
						<div class="description-item">
							<?=$item['description'];?>
						</div>
						<div class="category-item">Категория: <?=$item['name_category'];?></div>
						<div class="price-item">Цена: <span><?=$item['price'];?></span> руб</div>
					</div>
					<? endforeach;?>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2"><hr></td>
		</tr>
		<tr id="footer">
			<td class="copy">2016 &copy; Кондитерский концерн "Бабаевский"</td>
		</tr>
	</table>
</body>
</html>